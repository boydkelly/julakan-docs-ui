'use strict'

const stylelint = require('gulp-stylelint')
const postcss = require('gulp-postcss')
const vfs = require('vinyl-fs')

module.exports = (files) => (done) =>
  vfs
    .src(files)
    .pipe(postcss(stylelint({ reporters: [{ formatter: 'string', console: true }], failAfterError: true })))
    .on('error', done)
