document.addEventListener('DOMContentLoaded', function () {
  if (sessionStorage.getItem('dyu') === null) {
    fetchData()
      .then(() => {
        console.log('Data fetched');
        updateUI(); // Call updateUI when the data is ready
      })
      .catch(error => {
        console.error('Error fetching data:', error);
      });
  } else {
    updateUI(); // Call updateUI if data is already in sessionStorage
    //    checkNeodashAvailability();
  }
}, false);
