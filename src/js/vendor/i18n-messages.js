  var i18n = window.i18n();
  // include and initialize the rollbar library with your access token
  // i18n.setMessages(domain, locale, messages, plural_form);
  i18n.setMessages('messages', 'fr', {
      "Welcome": "Bienvenue",
      "Contents": "Sur cette page...",
      "Edit page": "Éditer cette page...",
      "Search the docs": "Rechercher...",
      "Study": "Étude",
      "Prev": "Précédent.",
      "Next": "Suivant",
      "Document language": "Langue du document",
      "Site language": "Langue du site",
      "There is %1 apple": [
          "Il y a %1 pomme",
          "Il y a %1 pommes"
        ]
    }, 'nplurals=2; plural=n>1;');
  i18n.setMessages('messages', 'dyu', {
      "Welcome": "I dan se",
      "Contents": "Kɔnɔnakow",
      "Edit page": "Sɛbɛ nin yɛlɛma",
      "Search the docs": "I be ɲini yan",
      "Study": "Karan",
      "Prev": "Kɔfɛta",
      "Next": "Nɔfɛta",
      "Document language": "Sɛbɛ kaan",
      "Site language": "Site web kaan",
      "There is %1 apple": [
          "Il y a %1 pomme",
          "Il y a %1 pommes"
        ]
    }, 'nplurals=2; plural=n>1;');
  i18n.setMessages('messages', 'bm', {
      "Welcome": "I dan se",
      "Contents": "Kɔnɔkow",
      "Edit page": "Sɛbɛ nin yɛlɛma",
      "Search the docs": "Ɲini",
      "Study": "Kalan",
      "Prev": "Yɔrɔ tɛmɛnnɛn",
      "Next": "Yɔrɔ nata",
      "Document language": "Sɛbɛ kaan",
      "Site language": "Site web kaan",
      "There is %1 apple": [
          "Il y a %1 pomme",
          "Il y a %1 pommes"
        ]
    }, 'nplurals=2; plural=n>1;');
