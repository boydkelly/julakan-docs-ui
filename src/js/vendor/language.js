// console.log('Starting JavaScript code');
window.addEventListener('DOMContentLoaded', function () {
  const paragraphs = document.querySelectorAll('.sect3 > .paragraph > p')
  for (const paragraph of paragraphs) {
    paragraph.setAttribute('lang', 'dyu')
  }

  const dtExamples = document.querySelectorAll('dt.hdlist1')
  for (const dtExample of dtExamples) {
    dtExample.setAttribute('lang', 'dyu')
  }
})
