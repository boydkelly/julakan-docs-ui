(function() {
  var input = document.getElementById("search-input")
  var searchText = i18n.gettext('Search the docs')
  input.placeholder = searchText
})();
// var brand = document.getElementsByClassName("navbar-brand")[0]
// var items = brand.getElementsByClassName("navbar-item"); (if the elements are just navbar-item(s) )
var items = document.getElementsByClassName("component");
for (let i = 0; i < items.length; i++) {
     // console.log(items[i].length)
     // console.log(items[i])
     // console.log(items[i].innerHTML)
  var title = i18n.gettext(items[i].innerHTML)
  items[i].innerHTML = title
}

var site = document.getElementById("site-lang")
var doc = document.getElementById("page-lang")
// console.log(doc)
site.title = i18n.gettext('Site language')

if (document.getElementById('page-lang') != null) {
  //    console.log("element exists");
  doc.title = i18n.gettext('Document language')
}
  //this will break if there are more than one
// var etp = document.getElementsByClassName("edit-this-page")[0]
// //  etp.textContent = i18n.gettext('Edit this page')
// if (etp.length > 0) {
//   etp.textContent = i18n.gettext('Edit this page')
// }

