
window.onload = function() {
  draw(); // Call the draw function
  console.log('Page has finished loading');
  neoViz.network.stabilize();
  // setInterval(function() {
  //   neoViz.network.fit();
  // }, 2000);
  // neoViz.network.fit();
}

window.onresize = function() {
  setInterval(function() {
    neoViz.network.fit();
  }, 2000);
};

document.addEventListener('DOMContentLoaded', function () {
  if (sessionStorage.getItem('dyu') === null) {
    fetchData();
  } else {
    updateUI();
    //    checkNeodashAvailability();
  }

  // draw(); // Call the draw function

  // setInterval(function() {
  //   neoViz.network.fit();
  // }, 2000);

  // neoViz.registerOnEvent('completed', (e) => {
  //   neoViz.network.stabilize();
  //   neoViz.network.fit();
  //
  // });

}, false);

